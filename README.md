# docker-maven-ktlint

A Docker image based on [`maven:3-jdk-8`](https://hub.docker.com/_/maven/) with [ktlint](https://ktlint.github.io) installed.

The resulting Docker image is automatically deployed to both [Docker Hub](https://hub.docker.com/r/drewsilcockstfc/maven-ktlint/) and this project's [container registry](https://gitlab.com/drewsberry/docker-maven-ktlint/container_registry).

All created Docker tags correspond to tags in this repository, with `latest` always being the most recent commit on the master branch.

## Spring Boot

This Docker image is provided in particular for using Kotlin to build Spring Boot applications.
